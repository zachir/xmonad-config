-- xmonad config used by ZachIR
-- Modified from config by Vic Fryzel
-- https://github.com/vicfryzel/xmonad-config

-- Imports {{{
import System.IO
import System.Exit
import XMonad hiding ( (|||) )
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.WorkspaceHistory
import XMonad.Layout.IndependentScreens
import XMonad.Layout.LayoutCombinators
import XMonad.Util.EZConfig
import XMonad.Util.Run(spawnPipe)
import Data.List (sortBy)
import Data.Function (on)
import Control.Monad (forM_, join)
import XMonad.Util.Run (safeSpawn)
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import Custom.MyKeybindings
import Custom.MyLayouts
import Custom.MyScratchpads
import Custom.MyVariables
import Custom.MyManageHook

--}}}
-- Workspaces {{{

-- Workspaces
-- Very simple, numbered 1 thru 9
myWorkspaces = map show [1..9]

--}}}
-- Startup Hook {{{

------------------------------------------------------------------------
-- Startup hook
-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
    spawn "sh ~/.config/autostart.sh"
    --spawn "stalonetray &"
    return ()

-- }}}
-- LogHook {{{

myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0

-- }}}
-- main {{{

------------------------------------------------------------------------
-- Run xmonad with all the defaults we set up.
--
main = do
--  dzenLeftBar <- spawnPipe myXmonadBar
--  dzenRightBar <- spawnPipe myStatusBar
--  defaults <- dzen defaults
    n <- countScreens
    xmprocs <- mapM (\i -> spawnPipe $ "xmobar ~/.xmonad/xmobarrc" ++ show i) [1..n]
    xmonad $ docks $ ewmhFullscreen $ ewmh $ defaults {
--      logHook = myLogHook dzenLeftBar >> fadeInactiveLogHook 0xdddddddd
        manageHook = manageDocks <+> myManageHook
        , handleEventHook = swallowEventHook (className =? "St" <||> className =? "Alacritty") (return True) <+> handleEventHook def
        , logHook = mapM_ (\handle -> workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP {
            ppCurrent           =   xmobarColor "#ebac54" "#1B1D1E" . pad
            , ppVisible           =   xmobarColor "white" "#1B1D1E" . pad
            , ppHidden            =   xmobarColor "white" "#1B1D1E" . pad
            , ppHiddenNoWindows   =   xmobarColor "#7b7b7b" "#1B1D1E" . pad
            , ppUrgent            =   xmobarColor "#ff0000" "#1B1D1E" . pad
            , ppWsSep             =   " "
            , ppSep               =   "  |  "
            , ppLayout            =   xmobarColor "#ebac54" "#1B1D1E" .
                                      (\x -> case x of
                                          "ResizableTall"             ->      "^i(" ++ myBitmapsDir ++ "/tall.xbm)"
                                          "Mirror ResizableTall"      ->      "^i(" ++ myBitmapsDir ++ "/mtall.xbm)"
      --                                  "Full"                      ->      "^i(" ++ myBitmapsDir ++ "/full.xbm)"
                                          "Simple Float"              ->      "~"
                                          _                           ->      x
                                      )
            , ppTitle             =   (" " ++) . xmobarColor "white" "#1B1D1E" . dzenEscape
            , ppOutput = hPutStrLn handle }) xmprocs
    }

-- }}}
-- defaults {{{

------------------------------------------------------------------------
-- Combine it all together
-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults = ewmh def {
    -- simple stuff
    terminal           = myTerminal,
    focusFollowsMouse  = myFocusFollowsMouse,
    borderWidth        = myBorderWidth,
    modMask            = myModMask,
    workspaces         = myWorkspaces,
    normalBorderColor  = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,

    -- key bindings
    keys               = myKeys,
    mouseBindings      = myMouseBindings,

    -- hooks, layouts
    layoutHook         = myLayout,
    manageHook         = myManageHook,
    startupHook        = myStartupHook
}
    `additionalKeysP`
    myAdditionalKeybindings

-- }}}
