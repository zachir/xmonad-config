module Custom.MyLayouts where

-- Imports
import XMonad hiding ( (|||) )
import XMonad.Hooks.ManageDocks
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Gaps
import XMonad.Layout.Fullscreen
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Named(named)
import XMonad.Layout.NoBorders

-- My layouts
-- Either 1) bsp, 2) tall, or 3) fullscreen layouts
myLayout =
    named "BSPL" (avoidStruts ( gaps [(U,10), (R,10), (D,10), (L,10)] $ emptyBSP )) |||
    named "Tall" (avoidStruts ( gaps [(U,10), (R,10), (D,10), (L,10)] $ Tall 1 (3/100) (1/2) )) |||
    named "Full" (noBorders (fullscreenFull Full))
