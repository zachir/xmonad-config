module Custom.MyScratchpads where

-- Imports
import XMonad hiding ( (|||) )
import XMonad.Layout.Named(named)
import XMonad.Util.NamedScratchpad
import qualified XMonad.StackSet as W

scratchpads :: [NamedScratchpad]
scratchpads = [ NS "htop" spawnHtop findHtop manageHtop
              , NS "term" spawnTerm findTerm manageTerm 
              , NS "pulsemixer" spawnMixer findMixer manageMixer
              , NS "bluetoothctl" spawnBtctl findBtctl manageBtctl
              , NS "ncmpcpp" spawnNcmpcpp findNcmpcpp manageNcmpcpp
              , NS "neomutt" spawnNeomutt findNeomutt manageNeomutt
    ]
      where
        spawnHtop = "st -c htop -e /usr/bin/htop"
        findHtop  = className =? "htop"
        manageHtop= customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
        spawnTerm = "st -c term"
        findTerm  = className =? "term"
        manageTerm= customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
        spawnMixer = "st -c pulsemixer -e pulsemixer"
        findMixer  = className =? "pulsemixer"
        manageMixer= customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
        spawnBtctl  = "st -c bluetoothctl -e bluetoothctl"
        findBtctl   = className =? "bluetoothctl"
        manageBtctl = customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
        spawnNcmpcpp  = "st -c ncmpcpp -e ncmpcpp"
        findNcmpcpp   = className =? "ncmpcpp"
        manageNcmpcpp = customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
        spawnNeomutt  = "st -c neomutt -e neomutt"
        findNeomutt   = title =? "neomutt"
        manageNeomutt = customFloating $ W.RationalRect l t w h
          where
            h = 0.6
            w = 0.6
            t = (1 - h)/2
            l = (1 - w)/2
