module Custom.MyVariables where

import XMonad

-- My terminal command
myTerminal :: String
myTerminal = "st"

-- The command to lock the screen or show the screensaver.
myScreensaver :: String
myScreensaver = "slockd"

-- The command to take a selective screenshot, where you select
mySelectScreenshot :: String
mySelectScreenshot = "select-screenshot"

-- The command to take a fullscreen screenshot.
myScreenshot :: String
myScreenshot = "maim"

-- The command to use as a launcher, to launch commands that don't have
-- preset keybindings.
myLauncher :: String
myLauncher = "dmenu_run"

-- bitmaps dir for dzen2
myBitmapsDir :: String
myBitmapsDir = "/home/my_user/.xmonad/bitmaps"

-- dzen2 launch commands
-- myXmonadBar = "dzen2 -x '0' -y '0' -w '683' -h '24' -ta 'l' -fg '#FFFFFF' -bg '#1B1D1E' -dock -fn mononoki"
-- myStatusBar = "dzenpepe | dzen2 -x '683' -y '0' -w '683' -h '24' -ta 'r' -fg '#FFFFFF' -bg '#1B1D1E' -fn mononoki"

-- My border colors
myNormalBorderColor :: String
myNormalBorderColor = "#67cb38"
myFocusedBorderColor :: String
myFocusedBorderColor = "#81ff47"

-- width of the window border in pixels
myBorderWidth :: Dimension
myBorderWidth = 2
