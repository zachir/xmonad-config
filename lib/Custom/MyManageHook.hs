module Custom.MyManageHook where

-- Imports
import XMonad
import XMonad.Hooks.ManageHelpers
import XMonad.Util.NamedScratchpad
import qualified XMonad.StackSet as W

import Custom.MyVariables
import Custom.MyScratchpads

-- Window rules
myManageHook = composeAll
        [ resource  =? "desktop_window" --> doIgnore
--      , className =? "Galculator"     --> doFloat
        , className =? "Steam"          --> doFloat
--      , className =? "Gimp"           --> doFloat
        , resource  =? "gpicview"       --> doFloat
--      , className =? "MPlayer"        --> doFloat
--      , className =? "VirtualBox"     --> doShift "4:vm"
--      , className =? "Xchat"          --> doShift "5:media"
--      , className =? "stalonetray"    --> doIgnore
        , className =? "trayer"         --> doIgnore
        , className =? "QjackCtl"       --> doFloat
        , className =? "Ardour-5.12.0"  --> doFloat
        , className =? "Nwgdmenu"       --> doFloat
        , isFullscreen --> (doF W.focusDown <+> doFullFloat)] <+> namedScratchpadManageHook scratchpads


