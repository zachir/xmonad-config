module Custom.MyKeybindings where

--Imports

import Graphics.X11.ExtraTypes.XF86
import System.IO
import System.Exit
import XMonad hiding ( (|||) )
import XMonad.Hooks.ManageDocks
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.LayoutCombinators
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.NamedScratchpad
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import Custom.MyScratchpads
import Custom.MyVariables
 
-- These are my key bindings
-- modkey is alt
myModMask = mod1Mask

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
  ----------------------------------------------------------------------
  -- Special Key Bindings
  --

  -- Start a terminal.  Terminal to start is specified by myTerminal variable.
  [ ((modMask, xK_Return),
    spawn $ XMonad.terminal conf)

  , ((modMask .|. controlMask, xK_z),
    namedScratchpadAction scratchpads "htop")

  , ((modMask .|. controlMask, xK_x),
    namedScratchpadAction scratchpads "term")

  , ((modMask .|. controlMask, xK_c),
    namedScratchpadAction scratchpads "pulsemixer")

  , ((modMask .|. controlMask, xK_v),
    namedScratchpadAction scratchpads "bluetoothctl")

  , ((modMask .|. controlMask, xK_b),
    namedScratchpadAction scratchpads "ncmpcpp")

  , ((modMask .|. controlMask, xK_a),
    namedScratchpadAction scratchpads "neomutt")

  , ((modMask .|. mod4Mask, xK_r),
    sendMessage Rotate)

  , ((modMask .|. mod4Mask, xK_s),
    sendMessage Swap)

  , ((modMask, xK_f),
    sendMessage $ JumpToLayout "Full")

  , ((modMask .|. shiftMask, xK_t),
    sendMessage $ JumpToLayout "Tall")

  , ((modMask, xK_t),
    sendMessage $ JumpToLayout "BSPL")

  --------------------------------------------------------------------
  -- "Standard" xmonad key bindings
  --

  -- Close focused window.
  , ((modMask .|. shiftMask, xK_q),
     kill)

  --  Reset the layouts on the current workspace to default.
  --, ((modMask .|. shiftMask, xK_space),
  --   setLayout $ XMonad.layoutHook conf)

  -- Resize viewed windows to the correct size.
  , ((modMask, xK_n),
     refresh)

  -- Move focus to the next window.
  , ((modMask, xK_Tab),
     windows W.focusDown)
  
  -- Move focus to previous window
  , ((modMask .|. shiftMask, xK_Tab),
    windows W.focusUp)

  -- Move focus to the next window.
  , ((modMask, xK_j),
     windows W.focusDown)

  -- Move focus to the previous window.
  , ((modMask, xK_k),
     windows W.focusUp  )

  -- Move focus to the master window.
  , ((modMask, xK_m),
     windows W.focusMaster  )

  -- Swap the focused window and the master window.
  , ((modMask .|. shiftMask, xK_Return),
     windows W.swapMaster)

  -- Swap the focused window with the next window.
  , ((modMask .|. shiftMask, xK_j),
     windows W.swapDown  )

  -- Swap the focused window with the previous window.
  , ((modMask .|. shiftMask, xK_k),
     windows W.swapUp    )

  -- Shrink the master area.
  , ((modMask, xK_h),
     sendMessage Shrink)

  -- Expand the master area.
  , ((modMask, xK_l),
     sendMessage Expand)

  -- Push window into floating
  --, ((modMask, xK_space),
  --   withFocused $ windows . W.float)

  -- Push window back into tiling.
  , ((modMask .|. shiftMask, xK_space),
     withFocused $ windows . W.sink)

  -- Increment the number of windows in the master area.
  , ((modMask .|. shiftMask, xK_h),
     sendMessage (IncMasterN 1))

  -- Decrement the number of windows in the master area.
  , ((modMask .|. shiftMask, xK_l),
     sendMessage (IncMasterN (-1)))

  -- Toggle the status bar gap.
  -- TODO: update this binding with avoidStruts, ((modMask, xK_b),
  , ((modMask, xK_b),
     sendMessage ToggleStruts)

  -- Quit xmonad.
  , ((modMask .|. shiftMask, xK_e),
     io (exitWith ExitSuccess))

  -- Restart xmonad.
  , ((modMask .|. shiftMask, xK_r),
     spawn "xmonad --restart")
  ]
  ++

  -- mod-[1..9], Switch to workspace N
  -- mod-shift-[1..9], Move client to workspace N
  [((m .|. modMask, k), windows $ f i)
      | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
  ++

  -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
  -- mod-shift-{i,o,p}, Move client to screen 1, 2, or 3
  [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip [xK_u, xK_i, xK_o] [0..]
      , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings
--
-- Focus rules
-- True if your focus should follow your mouse cursor.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $
  [
    -- mod-button1, Set the window to floating mode and move by dragging
    ((modMask, button1),
     (\w -> focus w >> mouseMoveWindow w))

    -- mod-button2, Raise the window to the top of the stack
    , ((modMask, button2),
       (\w -> focus w >> windows W.swapMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask, button3),
       (\w -> focus w >> mouseResizeWindow w))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
  ]


myAdditionalKeybindings :: [(String, X ())]
myAdditionalKeybindings =
  [ ("M-M4-<Left>",    sendMessage $ ExpandTowards L)
  , ("M-M4-<Right>",   sendMessage $ ShrinkFrom L)
  , ("M-M4-<Up>",      sendMessage $ ExpandTowards U)
  , ("M-M4-<Down>",    sendMessage $ ShrinkFrom U)
  , ("M-M4-S-<Left>",  sendMessage $ ShrinkFrom R)
  , ("M-M4-S-<Right>", sendMessage $ ExpandTowards R)
  , ("M-M4-S-<Up>",    sendMessage $ ShrinkFrom D)
  , ("M-M4-S-<Down>",  sendMessage $ ExpandTowards D)
  ]
